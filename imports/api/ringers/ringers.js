export const Ringers = new Mongo.Collection('Ringers');

Ringers.schema = new SimpleSchema({
	name: { type: String },
	incompleteCount: { type: Number, defaultValue: 0 },
	userId: { type: String, regEx: SimpleSchema.RegEx.Id, optional: true }
});

Meteor.startup(() => {
    Ringers._ensureIndex({ name: 1 });
});
