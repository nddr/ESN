export const Teams = new Mongo.Collection('Teams');

Teams.schema = new SimpleSchema({
	name: { type: String },
	tag: { type: String },
	url: { type: String },
	password: { type: String },
	created_on: { type: String },
	owner: { type: String, regEx: SimpleSchema.RegEx.Id },
	logo_url: { type: String, regEx: SimpleSchema.RegEx.Url },
	web_url: { type: String, regEx: SimpleSchema.RegEx.Url },
	is_recruiting: { type: Boolean, defaultValue: false },
	likes: { type: Number },
	removed: { type: Boolean, defaultValue: false },
	'server.ip': { type: String, regEx: SimpleSchema.RegEx.IP },
	'server.password': { type: String },
	'divisions.dota.comp_tier': { type: String },
	'divisions.dota.member_ids': { type: [String], regEx: SimpleSchema.RegEx.Id },
	'divisions.csgo.comp_tier': { type: String },
	'divisions.csgo.member_ids': { type: [String], regEx: SimpleSchema.RegEx.Id }
});

Meteor.startup(() => {
    Teams._ensureIndex({ userId: 1 });
});
