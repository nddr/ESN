export const Agents = new Mongo.Collection('Agents');

Agents.schema = new SimpleSchema({
	name: { type: String },
	incompleteCount: { type: Number, defaultValue: 0 },
	userId: { type: String, regEx: SimpleSchema.RegEx.Id, optional: true }
});

Meteor.startup(() => {
    Agents._ensureIndex({ userId: 1 });
});
