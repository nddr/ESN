export const Scrims = new Mongo.Collection('Scrims');

Scrims.schema = new SimpleSchema({
	user_id: { type: String, regEx: SimpleSchema.RegEx.Id },
	contact: { type: String },
	date: { type: String },
	status: { type: Number, defaultValue: 0 },
	removed: { type: Boolean, defaultValue: false },
	'team.name': { type: String },
	'team.url': { type: String },
	'options.skill': { type: String },
	'options.region': { type: String },
	'options.password': { type: String, min: 3 }
});

Meteor.startup(() => {
    Scrims._ensureIndex({ date: 1 });
});
