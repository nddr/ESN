Slingshot.fileRestrictions('foodPhotos', {
    allowedFileTypes: ['image/png', 'image/jpeg'],
    maxSize: 2 * 1024 * 1024 // 2 MB (use null for unlimited).
});
