const title = 'ESN - ESports Scrim Network';
DocHead.setTitle(title);

const metaInfo = { name: 'description', content: 'Create a team, recruit players, find scrims & ringers' };
DocHead.addMeta(metaInfo);
