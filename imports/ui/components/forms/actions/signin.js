Template.SignIn.events({
	'submit form': function(e, tmpl) {
		e.preventDefault();
		
		var email = tmpl.find('#email').value,
			password = tmpl.find('#password').value;

		check(password, String);

		Meteor.loginWithPassword(email, password, function(err){
			if (err) {
				Session.set("formError", err.reason);
			} else {
				FlowRouter.go('/');
			}
		});
	}
});