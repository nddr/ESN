Template.RequestPassword.events({
	'submit form': function(e, tmpl) {
		e.preventDefault();

		var email = tmpl.find('#email').value;

		Meteor.subscribe('userData', email);
		//throttle this
		setTimeout(function(){
			var user = Meteor.users.findOne({ emails: { $elemMatch: { 'address': email }}});

			if (user) {
				Meteor.call('resetPasswordEmail', user._id);
				//use sAlert
				Session.set('formError', 'You should recieve an email shortly.');
			} else {
				Session.set('formError', 'Email not found. Are you sure it is correct?');
			}
		}, 1000);
	}
});