Template.SignUp.events({
	'submit form': function(e, tmpl) {
		e.preventDefault();

		var options,
			email = tmpl.find('#email').value,
			handle = tmpl.find('#handle').value,
			password = tmpl.find('#password').value,
			confirm = tmpl.find('#confirm').value,
			steamId = tmpl.find('#steam').value,
			xblId = tmpl.find('#xbl').value,
			fname = tmpl.find('#fname').value,
			lname = tmpl.find('#lname').value;

		//check([handle, password, confirm, steamId, xblId, fname, lname], [String]);

		options = {
			email: email,
			password: password,
			profile: {
				first_name: fname,
				last_name: lname,
				handle: handle,
				photo_url: null,
				hometown: null,
				birthdate: null,
				summary: null,
				likes: 0,
				steamId: steamId,
				xblId: xblId,
				team: {
					name: null,
					url: null,
					logo_url: null,
					role: null
				},
				game: {
					fav_heroes: [],
					fav_weapons: [],
					fav_map: null
				},
				reputation: {
					score: null,
					skill: null,
					comms: null,
					posture: null
				},
				social: {
					twitter: null,
					youtube: null,
					twitch: null
				}
			}
		}

		if (password === confirm) {
			Accounts.createUser(options, function(err) {
				if (err) {
					Session.set('formError', err.reason);
				} else {
					FlowRouter.go('/');
				}
			});
		} else {
			Session.set('formError', 'Password mismatch');
		}
	}
});