import React from 'react';
import GlobalNavigation from './global_navigation.js';
import UserNavigation from './user_navigation.js';

const MainLayout = ({ content }) => (
	<div className="MainLayout">
		<GlobalNavigation />
		<UserNavigation />
		<main>
			{content}
		</main>
	</div>
);

MainLayout.propTypes = {
	content: React.PropTypes.string.isRequired
};

export default MainLayout;
