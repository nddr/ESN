import React from 'react';

const UserNavigation = () => {
    // if (game) {
    //     return (
    //         <div id="dota-content-nav" className="content-nav">
    //             <div id="content-menu">
    //                 <img src="/dota.png" height="42" width="42" />
    //                 <span id="content-title">dota 2</span>
    //                 <ul>
    //                     <li><a href="/dota/">news</a></li>
    //                     <li><a href="/dota/scrim">scrims</a></li>
    //                     <li><a href="/dota/ringer">ringers</a></li>
    //                     <li><a href="/dota/recruit">recruiting</a></li>
    //                 </ul>
    //             </div>
    //         </div>
    //     );
    // }

    return (
        <div className="UserNav">
            <header>E-Sports Scrim Network</header>
            <div className="Search">
                <input type="text" placeholder="Search..." />
            </div>
            <div className="Notifications">

            </div>
            <div className="UserPortrait">
                <img src="" />
            </div>
        </div>
    );
};

export default UserNavigation;
