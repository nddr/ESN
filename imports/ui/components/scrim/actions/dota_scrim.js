Template.DotaScrim.events({
    'submit form': function(e, tmpl){
		e.preventDefault();
        var region = tmpl.find('#region').value,
            skill = tmpl.find('#skill').value;

		var scrim = {
      		region: region,
            skill: skill,
      		password: tmpl.find('#password').value
        };

        //check if user has steam ID
        if (Meteor.user().profile.steamId) {
            //check if user chose a region and skill
            if (region && skill) {
                Meteor.call('createDotaScrim', scrim);
                tmpl.find('#password').value = '';
            } else {
                alert('Select a region and skill level.');
            }
        } else {
            alert('Update profile: Steam account is required.');
        }
    }
});

Template.DotaScrim.helpers({
    isDisabled: function() {
        if (!Meteor.user()) {
            return true;
        } else {
            // if client already has an active scrim
        }
    },

    scrims: function() {
        //requries further testing
        var max = moment().utc().add(1, 'h').format();
        var min = moment().utc().subtract(1, 'h').format();
        return DotaScrims.find({ time: { $gt: min, $lt: max }}, { sort: { time: -1 }});
    }
});