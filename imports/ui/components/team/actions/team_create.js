Template.TeamCreate.events({
	'submit form': function(e, tmpl) {
		e.preventDefault();

		var password = tmpl.find('#password').value,
			confirm = tmpl.find('#confirm').value,
			url = tmpl.find('#url').value,
			name = tmpl.find('#name').value,
			tag = tmpl.find('#tag').value;

		check([password, url, name, tag], [String]);

		var team = {
			url: url,
			name: name,
			tag: tag,
			password: password,
			owner: Meteor.user()._id,
			members: [
				{
					first_name: Meteor.user().profile.first_name,
					last_name: Meteor.user().profile.last_name,
					handle: Meteor.user().profile.handle,
					img_url: Meteor.user().profile.img_url,
					division: null,
					role: null,
					captain: true
				}
			]
		};
		
		//if URL is present, check if its unused
		var search = Teams.findOne({ url: url });
		
		if (!search) {
			if (password === confirm) {
				Meteor.call('createTeam', team);
			} else {
				Session.set('formError', 'Password mismatch.');
			}
		} else {
			Session.set('formError', 'URL is taken. Try another.');
		}
	}
});