import React from 'react';

const Search = () => (
	<div className="Search">
		<span className="search-icon"></span>
		<input type="text" className="" placeholder="Search" />

		<ul>
			<li></li>
		</ul>
	</div>
);

export default Search;
